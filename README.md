# Atlas pH - I2C for Espruino

Javascript module for Espruino exposing the i2c interface to the
Atlas Scientific pH Sensor (technically the EZO circuit)[See the data sheet for details.](https://www.atlas-scientific.com/_files/_datasheets/_circuit/pH_EZO_Datasheet.pdf).

## notes / errata

Not all features are supported. For example, no resetting the EZO circuit back to UART.
I tried to keep it to what was useful. Useful is, of course, relative to me, so...

Most functions require a callback if data is going to be returned. If no data is expected,
no callback is made. This could change in the future because all commands have
a return value.

## usage

### a basic read
```javascript
var atlasPH = require("atlas-ph");
I2C1.setup({slc:D17,sda:D16});
var ph = atlasPH(100, I2C1);
ph.read(m=>console.log(m));
```

### midpoint calibration
```javascript
var atlasPH = require("atlas-ph");
I2C1.setup({slc:D17,sda:D16});
var ph = atlasPH(100, I2C1);
ph.calibrate("mid");
```

### low point calibration
```javascript
var atlasPH = require("atlas-ph");
I2C1.setup({slc:D17,sda:D16});
var ph = atlasPH(100, I2C1);
ph.calibrate("low");
```

### high point calibration
```javascript
var atlasPH = require("atlas-ph");
I2C1.setup({slc:D17,sda:D16});
var ph = atlasPH(100, I2C1);
ph.calibrate("high");
```

## Troubleshooting
If you're having trouble getting readings, *and* you're not using the EZO Inline Isolator, I would recomend it.

If you are using the EZO Inline Isolator (or, have a comparable isolator circuit already), *and* you're still
having problems getting a reading, do the regular stuff like check connections, cables, etc. After that, take
a look at the code and it's a fairly simple wrapper around all the commands. You can check on return codes
manually if you execute the i2c alone.