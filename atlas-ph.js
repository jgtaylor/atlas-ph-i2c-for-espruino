"use strict";
// Support Cal, Factory, Find, i, L, R, RT,_temp, Slope, Status, T
module.exports = function ( address, i2cInstance ) {
  function ua2text( ua ) {
    var s = "";
    for ( var i = 1; i < ua.length; i++ ) {
      if ( ua[i] !== 0x01 && ua[i] !== 0x00 ) {
        s += String.fromCharCode( ua[i] );
      }
    }
    return s;
  }
  function checkReturnStatus( rByte ) {
    if ( rByte == 1 ) { return "Success"; }
    if ( rByte == 2 ) { return "syntax error"; }
    if ( rByte == 254 ) { return "still processing, not ready"; }
    if ( rByte == 255 ) { return "no data to send"; }
  }

  return {
    calibrate: function ( type, cb ) {
      let _cmdString;
      if ( type == "mid" ) {_cmdString = "Cal,mid,7.00";}
      if ( type == "low" )  {_cmdString = "Cal,low,4.00";}
      if ( type == "high" )  {_cmdString = "Cal,high,10.00";}
      i2cInstance.writeTo( address, _cmdString );
      ( cb ) && setTimeout( () => {
        let data = i2cInstance.readFrom( address, 1 );
        cb( checkReturnStatus( data ) );
      }, 900 );
      return true;
    },
    factory: function () {
      i2cInstance.writeTo( address, "Factory" );
      return true;
    },
    find: function () {
      i2cInstance.writeTo( address, "Find" );
      return true;
    },
    info: function ( cb ) {
      if ( !cb ) { return new Error( "info requires a callback" ); }
      i2cInstance.writeTo( address, "i" );
      setTimeout( () => {
        let data = i2cInstance.readFrom( address, 31 );
        let _s = ua2text( data ).split( "," );
        cb( { unit: _s[1], firmware: _s[2] } );
      }, 300 );
      return true;
    },
    LED: function ( state ) {
      if ( state == true ) {
        state = 1;
      } else {
        state = 0;
      }
      let _cmdString = "L," + state;
      i2cInstance.writeTo( address, _cmdString );
      return true;
    },
    read: function ( cb ) {
      if ( !cb ) { return new Error( "read requires a callback" ); }
      i2cInstance.writeTo( address, "R" );
      setTimeout( () => {
        let data = i2cInstance.readFrom( address, 31 );
        cb( { pH: parseFloat( ua2text( data ) ) } );
      }, 900 );
      return true;
    },
    slope: function ( cb ) {
      if ( !cb ) { return new Error( "slope requires a callback" ); }
      i2cInstance.writeTo( address, "Slope,?" );
      setTimeout( () => {
        let data = i2cInstance.readFrom( address, 31 );
        let _s = ua2text( data ).split( "," );
        cb( { acid: parseFloat( _s[1] ), base: parseFloat( _s[2] ) } );
      }, 300 );
      return true;
    },
    status: function ( cb ) {
      if ( !cb ) { return new Error( "status requires a callback" ); }
      i2cInstance.writeTo( address, "Status" );
      setTimeout( () => {
        let data = i2cInstance.readFrom( address, 31 );
        let _s = ua2text( data ).split( "," );
        cb( { restartCode: _s[1], voltage: parseFloat( _s[2] ) } );
      }, 300 );
      return true;
    },
    sleep: function () {
      i2cInstance.writeTo( address, "Sleep" );
      return true;
    },
    readTempCompensated: function ( temp, cb ) {
      if ( !cb ) { return new Error( "readTempCompensated requires a callback" ); }
      i2cInstance.writeTo( address, "RT," + temp );
      setTimeout( () => {
        let data = i2cInstance.readFrom( address, 31 );
        cb( { pH: parseFloat( ua2text( data ) ) } );
      }, 900 );
      return true;
    },
    setTemperatureCompensation: function ( temp, cb ) {
      i2cInstance.writeTo( address, "T," + temp );
      return true;
    }
  };
};